#!/bin/bash
# Author: dm_2008@mail.ru
# distribution: freeware (unlicense)

# Разделитель и таблица
coldelim=";"
rowdelim="\n"
nm="test"
srv="192.168.0.1"

# База и схема
db="NorthWind"
schema="dbo"

# Формирование файла формата
bcp "$schema.$nm" format null \
    -d "$db" -S "$srv" -U $(<./_msusr) -P $(<./_mspas) \
    -t "$coldelim"\
    -r "$rowdelim"\
    -c -f "$nm.fmt"

rm "null"
