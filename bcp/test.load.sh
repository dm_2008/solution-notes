#!/bin/bash

nm="test"
fn="./test.dat"
fmt="$nm.fmt"

db="NorthWind"
schema="dbo"
srv="192.168.0.1"

tb="$schema.$nm"

#echo "Очистка таблицы..."
sqlcmd -w 10000 -r -b \
    -U $(<./_msusr) -P$(<~/.sqlsrvpass) \
    -S "$srv"  -d "$db" \
    -Q"truncate table $tb"

echo "Загрузка данных..."
bcp "$tb" in "$fn" \
    -U $(<./_msusr) -P $(<./_mspas) \
    -S "$srv"  -d "$db" \
    -F 2 -e "$nm.err" -f "$fmt" \

sqlcmd -w 10000 -r -b \
    -U $(<./_msusr) -P$(<~/.sqlsrvpass) \
    -S "$srv"  -d "$db" \
    -Q"select count (1) cnt from $tb"
