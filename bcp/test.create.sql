-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

if object_id('dbo.test') is not null
    drop table dbo.test
GO
create table dbo.test (
      RowID int identity (1,1)
    , id            int
    , description   varchar(255)
    , dt_str        varchar(255)
    , dt            datetime2(0)
    , val           int
)
GO
select top 10 * from dbo.test
