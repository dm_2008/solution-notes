﻿-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Формирование тестовых данных */
create table Pool(
       pool_id             int not null,
       iin_type     varchar(50) not null,
       iin_start    int not null,
       iin_end             int not null
       constraint pk_Pool primary key(pool_id)
)
insert into Pool (pool_id, iin_type, iin_start, iin_end) 
values 
  (1, 'National', 300000,300099)
, (2, 'International', 400000,400199)
, (3, 'International', 400500,400999)
GO
 
create table PoolExcept(
       pool_id             int not null,
       iin_start    int not null,
       iin_end             int not null
)
GO

insert into PoolExcept (pool_id, iin_start, iin_end) 
values 
  (3,400505,400509)
, (3,400600,400700)
GO
 
create table iin(
       id           int identity not null,
       iin_number   int not null,
       company_name varchar(50) not null,
       registered_date     datetime not null,
       iin_type     varchar(50) not null,
       card_type    varchar(50) not null,
       revert_date  datetime null,
       constraint pk_iin primary key(id)
)
GO

truncate table iin;
insert into iin(iin_number, company_name, registered_date, iin_type, card_type, revert_date) 
values
  (400002,'British Airways', '20100101','International', 'Credit Card', '20120101')
, (400003,'British Airways', '20100101','International', 'Credit Card', null)
, (400004,'British Airways', '20100101','International', 'Credit Card', null)
, (400005,'British Airways', '20100101','International', 'Credit Card', null)
, (400015,'British Airways', '20110101','International', 'Credit Card', null)
, (400516,'British Airways', '20110101','International', 'Credit Card', null)
, (400517,'British Rail', '20110101','International', 'Debit Card', null)
, (400555,'British Rail', '20110101','International', 'Debit Card', null)
, (300001,'Diners Club', '20110101','National', 'ATM Card', null)
, (300099,'Diners Club', '20110101','National', 'ATM Card', null)
*/

/* Удаление объектов
drop table Pool;
drop table PoolExcept;
drop table iin;
*/
