-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Решение задачи                    */

-- Граничные параметры отчета
declare @startDt date = '2020-01-01';
declare @endDt date = '2020-04-01';

-- Формирование динамического SQL
declare @cols nvarchar(1000) = '';
declare @colsn nvarchar(1000) = '';
declare @sqlcmd nvarchar(4000) = '';

-- Список столбцов для динамического SQL
with s as (
    select distinct 
        convert (nvarchar(10), recdate, 103) ld
      , convert (nvarchar(10), recdate, 3) sd
      , recdate d
    from dbo.sales s
    where s.recdate between @startDt and @endDt
)
select @cols = concat (@cols, ',[', ld, ']')
, @colsn = concat (@colsn, ', isnull([', ld, '],0) as [', sd, ']')
from s
order by s.d asc;

-- Тело запроса
select @cols = right (@cols, len(@cols)-1)
, @colsn = right (@colsn, len(@colsn)-1)
, @sqlCmd = concat (
      N'with dat as (', char(10)
    , N'    select s.recdate', char(10)
    , N'    , s.product_id', char(10)
    , N'    , p.productName', char(10)
    , N'    , s.quantity q', char(10)
    , N'    from dbo.sales s', char(10)
    , N'        inner join dbo.products p on p.product_id = s.product_id', char(10)
    , N'    where s.recdate between @startDt and @endDt', char(10)
    , N')', char(10)
    , N'select productName, ', char(10)
    , @colsn, char(10)
    , N'from dat s', char(10)
    , N'pivot (', char(10)
    , N'    sum(q)', char(10)
    , N'    for recdate in (', char(10)
    , @cols
    , N')', char(10)
    , N') as ps', char(10)
    , N';', char(10)
);

exec sp_executesql @sqlCmd
, N'@startDt date, @endDt date', @startDt = @startDt, @endDt = @endDt

