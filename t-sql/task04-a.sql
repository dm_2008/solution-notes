-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Решение задачи                    */
select top 10
      usr.FirstName
    , usr.Surname
    , usr.LastName
    , usr.number
    , usr.ReceiveDate
from dbo.documents usr
    outer apply (
        select sum (try_convert ( int, substring(a.b, v.number+1, 1) ) ) i
        from (select usr.number b) a
            join master..spt_values v on v.number < len(a.b)
        where v.type = 'P'
    ) n1
    outer apply (
        select sum (try_convert ( int, substring(a.b, v.number+1, 1) ) ) i
        from (select cast (n1.i as varchar(10)) b) a
            join master..spt_values v on v.number < len(a.b)
        where v.type = 'P'
    ) n2
    outer apply (
        select sum (try_convert ( int, substring(a.b, v.number+1, 1) ) ) i
        from (select cast (n2.i as varchar(10)) b) a
            join master..spt_values v on v.number < len(a.b)
        where v.type = 'P'
    ) n3
where n3.i %2 = 0
order by usr.ReceiveDate asc

