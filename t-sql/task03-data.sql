﻿-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Формирование данных */
create table tps (
      date datetime2(0)
    , type int
    , qty money
)
GO

truncate table tps;
insert into tps (date, type, qty)
values
  ('2012-07-29', 0, 26)
, ('2012-08-24', 1, 2)
, ('2012-08-26', 1, 2)
, ('2012-08-28', 0, 94)
, ('2012-11-30', 1, 2)
, ('2012-12-02', 0, 29)
, ('2012-12-31', 0, 18)
, ('2013-01-18', 1, 3)
, ('2013-01-21', 0, 11)
, ('2013-02-01', 0, 1)
*/

/* Удаление данных
drop table tps
*/
