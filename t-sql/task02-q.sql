﻿-- Задача 2

/* Условия задачи

Диапазоны возможных номеров Pool
pool_id     iin_type                                           iin_start   iin_end
----------- -------------------------------------------------- ----------- -----------
1           National                                           300000      300099
2           International                                      400000      400199
3           International                                      400500      400999
 
-- Ислючения из диапазона PoolExcept
pool_id     iin_start   iin_end
----------- ----------- -----------
3           400505      400509
3           400600      400700
 
-- Зарегистрированные номера (iin)
--IIN - Issuer Identification Number
id          iin_number  company_name                                       registered_date         iin_type                                           card_type                                          revert_date
----------- ----------- -------------------------------------------------- ----------------------- -------------------------------------------------- -------------------------------------------------- -----------------------
1           400002      British Airways                                    2010-01-01 00:00:00.000 International                                      Credit Card                                        2012-01-01 00:00:00.000
2           400003      British Airways                                    2010-01-01 00:00:00.000 International                                      Credit Card                                        NULL
3           400004      British Airways                                    2010-01-01 00:00:00.000 International                                      Credit Card                                        NULL
4           400005      British Airways                                    2010-01-01 00:00:00.000 International                                      Credit Card                                        NULL
5           400015      British Airways                                    2011-01-01 00:00:00.000 International                                      Credit Card                                        NULL
6           400516      British Airways                                    2011-01-01 00:00:00.000 International                                      Credit Card                                        NULL
7           400517      British Rail                                       2011-01-01 00:00:00.000 International                                      Debit Card                                         NULL
8           400555      British Rail                                       2011-01-01 00:00:00.000 International                                      Debit Card                                         NULL
9           300001      Diners Club                                        2011-01-01 00:00:00.000 National                                           ATM Card                                           NULL
10          300099      Diners Club                                        2011-01-01 00:00:00.000 National                                           ATM Card                                           NULL
 
Необходимо вывести диапазоны свободных номеров
Например:
pool_id     iin_type                                           iin_range            count_of
----------- -------------------------------------------------- -------------------- -----------
1           National                                           300000 - 300000      1
1           National                                           300002 - 300098      97
2           International                                      400000 - 400002      3
2           International                                      400006 - 400014      9
2           International                                      400016 - 400199      184
3           International                                      400500 - 400504      5
3           International                                      400510 - 400515      6
3           International                                      400518 - 400554      37
3           International                                      400556 - 400599      44
3           International                                      400701 - 400999      299
 
*/
