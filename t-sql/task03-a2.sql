﻿-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Решение задачи           */
/* с использованием курсора */
if object_id('tempdb..#raw') is not null
	drop table #raw
create table #raw (date date, type int, qty float)

declare db_lst cursor for 
	select date, type, qty
	from tps order by date

declare @date date, @type int, @qty float
declare @prev_type int, @acc float = 0, @min_date date, @ins bit = 0

open db_lst  
fetch next FROM db_lst INTO @date, @type, @qty
select @prev_type = @type, @min_date = @date

while @@fetch_status = 0  
begin  
	if @type = @prev_type
		select @acc = @acc + @qty, @ins=0
	if @type <> @prev_type
	begin
		insert into #raw (date, type, qty)
		values (@min_date, @prev_type, @acc)

		select @min_date = @date, @acc = @qty, @ins = 1
	end
	select @prev_type = @type
		
    fetch next from db_lst into @date, @type, @qty

	print @date
	print @min_date
	print @ins
	print @@fetch_status
end 
insert into #raw (date, type, qty)
values (@min_date, @prev_type, @acc)

close db_lst  
deallocate db_lst

select * from #raw order by date

