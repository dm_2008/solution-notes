﻿-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Решение задачи                    */
/* с использованием оконных функций  */
with lstd as (
	select 
	  row_number () over (partition by type order by date) rn
	, row_number() over (order by date) rnd
	, type
	, date
	, qty
	from tps
)
select min (date) min_date, type, sum(qty)
from lstd
group by (rnd - rn), type
order by min_date
