﻿-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)


/* Решение */
-- pool_id     iin_type     iin_range            count_of
-- ----------- ------------ -------------------- -----------
-- 1           National     300000 - 300000      1

with lst as (
	select nn.rn - row_number() over (order by rn) grp
	, p.iin_start, p.iin_end, nn.rn, p.pool_id, p.iin_type
	from Pool p
		cross apply (
			select top (iin_end - iin_start + 1) row_number() over (order by (select 1)) + p.iin_start - 1 rn
			from sys.syscolumns obj1 with (nolock) cross join sys.syscolumns obj2 with (nolock)
		) nn
		left join PoolExcept e on nn.rn between e.iin_start and e.iin_end
		left join iin on nn.rn = iin.iin_number
where e.pool_id is null and iin.iin_number is null
)

select pool_id, iin_type, concat (min(rn), ' - ', max(rn)) iin_range, count (1) cnt
from lst
group by grp, pool_id, iin_type

