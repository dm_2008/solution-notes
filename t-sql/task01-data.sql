﻿-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Формирование тестовых данных */
create table Rate(
       id     int not null,
       code   varchar(50) not null,
       date   datetime not null,
       value  float not null
)
GO

truncate table Rate; 
insert into rate (id, code, date, value) 
values 
 (1,'EUR','20090604', 1.149)
,(2,'EUR','20090615', 1.161)
,(3,'EUR','20090617', 1.177)
,(4,'USD','20090605', 1.625)
,(5,'USD','20090615', 1.639)
,(6,'USD','20090617', 1.644)
GO

create table Calendar (
       dDay datetime not null,
)
GO

set datefirst 1;
truncate table dbo.Calendar;
insert into Calendar (dDay) 
select top 365 wdt
from (
	select dateadd(dd, row_number() over (order by(select 1)), '20090530') wdt
	from sys.sysobjects with (nolock)
 ) enum
-- Только рабочие дни
where datepart (dw, enum.wdt) < 6

/* Удаление объектов
drop table Rate; 
drop table Calendar;
*/
