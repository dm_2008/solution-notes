﻿-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Решение */
-- code     dDay                    value     weekday
-- -------- ----------------------- --------- -------
-- EUR      2009-06-05 00:00:00.000 1.149      Friday

select r.code, f.dDay, r.value, datename(dw, f.dDay) weekday
from Calendar f
cross apply (
	select top 1 with ties *
	from dbo.Rate r
	where r.date <= f.dDay
	order by row_number() over (partition by code order by date desc)
) r
order by r.code, f.dDay

