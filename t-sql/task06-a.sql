-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Решение задачи                    */

with 
  tStart as (
    select 
      date
    , lead (date, 1, '99990101') over (partition by id, Reason order by date asc) nextDate
    , id
    , Reason
    from tlog
    where Reason = 'Отбытие'
)

, dur as (
    select tStart.id
        , tStart.date startDate
        , tFin.date   endDate
        , datediff(mi, tStart.date, tFin.date) / 60. durH
    from tStart
        left join tLog tFin on tFin.id = tStart.id 
            and tFin.Reason = 'Прибытие'
            and tFin.date between tStart.date and tStart.nextDate
)
select id
    , cast (
        avg(durH) as decimal (16, 2)
    ) avgHour
from dur
group by id
