﻿-- Author: dm_2008@mail.ru
-- distribution: freeware (unlicense)

/* Создание структуры */
create table dbo.sales (
      tid int
    , product_id int
    , quantity float
    , recdate datetime2(0)
)

create table dbo.products (
      product_id int
    , productName nvarchar(15)
)

/* Формирование данных */
truncate table dbo.sales;
insert into dbo.sales
values
    (1, 100, 3, '2020-01-01'),(2, 100, 4, '2020-01-01'),(3, 100, 7, '2020-01-03')
   ,(11, 200, 2, '2020-02-01'),(12, 200, 6, '2020-02-03'),(13, 200, 4, '2020-02-03')
   ,(14, 200, 2, '2020-03-01'),(15, 200, 6, '2020-03-03'),(16, 200, 4, '2020-03-04')
;

truncate table dbo.products;
insert into dbo.products 
values
    (100, 'Кружка') ,(200, 'Ложка') ,(300, 'Нож')
  , (110, 'Бутылка') ,(210, 'Закуска') ,(310, 'Палатка')
;



select * from dbo.sales
select * from dbo.products

/* Удаление данных
drop table dbo.sales
drop table dbo.products
*/
